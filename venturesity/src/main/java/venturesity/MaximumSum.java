package venturesity;

import java.util.ArrayList;
import java.util.List;

public class MaximumSum {

	public static int generateMaximumSum(List<Integer> numbers) {
		if(numbers.size() == 0 || numbers.isEmpty() || numbers == null){
			return 0;
		}
		int sum1 = numbers.get(0);
		int sum2 = 0;
		int temproarySum;
		for (int i = 1; i < numbers.size(); i++) {
			temproarySum = (sum1 > sum2) ? sum1 : sum2;
			sum1 = sum2 + numbers.get(i);
			sum2 = temproarySum;
		}
		return ((sum1 > sum2) ? sum1 : sum2);
	}

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		System.out.println(generateMaximumSum(list));
	}

}
